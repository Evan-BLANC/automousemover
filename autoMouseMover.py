"""
Auto 🐭 Mover by Evan BLANC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to use the Software for personal, non-commercial purposes only.
All other uses are strictly prohibited.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
"""

# !/usr/bin/env python
import tkinter as tk
import time
import random
from threading import Thread
import pyautogui


def move_mouse():
    while running:
        x, y = random.randint(0, 1920), random.randint(0, 1080)
        pyautogui.moveTo(x, y)
        time.sleep(timeout)


def increase_timeout():
    current_value = int(timeout_entry.get())
    new_value = current_value + 5
    timeout_entry.delete(0, tk.END)
    timeout_entry.insert(0, str(new_value))


def decrease_timeout():
    current_value = int(timeout_entry.get())
    new_value = max(0, current_value - 5)
    timeout_entry.delete(0, tk.END)
    timeout_entry.insert(0, str(new_value))


def on_closing():
    global running
    running = False
    root.destroy()


def start_stop():
    global running
    if running:
        running = False
        button.config(text="▶️", bg="green")
    else:
        global timeout
        timeout = float(timeout_entry.get())
        running = True
        Thread(target=move_mouse, daemon=True).start()
        button.config(text="⏸", bg="red")


root = tk.Tk()
root.title("Dynamic mouse mover by Evan BLANC")
root.geometry("350x150")

# Create title label
title_label = tk.Label(root, text="Auto 🐭 Mover", font=("Helvetica", 16, "bold"))
title_label.pack(pady=10)

# Play Pause Button
button = tk.Button(root, text="▶️", command=start_stop, bg="green", fg="white", font=("Helvetica", 12, "bold"))
button.pack(pady=5)

# Timeout Input
timeout_label = tk.Label(root, text="Timeout (seconds):")
timeout_label.pack()

timeout_frame = tk.Frame(root)
timeout_frame.pack(pady=5)

timeout_entry = tk.Spinbox(timeout_frame, from_=0, to=1000, increment=5, width=5, font=("Helvetica", 12))
timeout_entry.pack(side=tk.LEFT)

timeout_entry.delete(0, tk.END)
timeout_entry.insert(0, "30")

running = False

root.protocol("WM_DELETE_WINDOW", on_closing)

# Start the Tkinter event loop
root.mainloop()
