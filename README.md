**Auto 🐭 Mover by Evan BLANC**

**Download**

To download Auto 🐭 Mover, choose the appropriate version for your operating system:

[Download for macOS](https://gitlab.com/Evan-BLANC/automousemover/uploads/8d8624a7b7d55e692e62a3ab4f950e0a/autoMouseMover)

Download for Windows


**Installation**

For macOS: 
-save file in a new directory
-open terminal and go to directory with 'cd path_directory'
-execute command 'chmod +x autoMouseMover' 
-right click and open
-allow access for 'Terminal'


**Overview**

Auto 🐭 Mover is a simple Python application designed to automatically move your mouse cursor at random intervals, providing a dynamic and active presence. This tool can be used for personal, non-commercial purposes only.


**Usage**

To use Auto 🐭 Mover, follow these steps:

Run the application by executing the provided script.
Click the "▶️" button to start the mouse movement.
To pause the movement, click the "⏸" button.
Adjust the timeout (interval between movements) using the "Timeout (seconds)" input field. Default timeout is 30 seconds.
Permissions

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to use the Software for personal, non-commercial purposes only. All other uses are strictly prohibited.


**Disclaimer**

THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT, OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


**Author**

Author: Evan BLANC
Important Note

Unauthorized distribution of this software is strictly prohibited. If you find this tool useful, please direct others to the original source for download.


**Acknowledgments**

Special thanks to the community for support and feedback.

Feel free to contribute or report issues on the GitHub Repository.
